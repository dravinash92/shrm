<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'shrm' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}/<1&:zHl0CneWH<,Rcjc,e}A2)lIjk@/?`*aNSKR MTW7VnnWLJ?i&HSZrw&Qqw' );
define( 'SECURE_AUTH_KEY',  'mBm< |TJt%!fJ!xw@^0-t.H+YTLdha@)P+EePIo<9xfj@70p1?8J1qT/!ok[6|vk' );
define( 'LOGGED_IN_KEY',    'lfkXJsTKm29CRuV83 5Z>m`WraYSzqq2N&H=M:$l?SL^hi4iu!RM1)e}Z@v%1{8Q' );
define( 'NONCE_KEY',        '7qDG*zd ZW=Y}>9%gmN(YZE#U brdxs}sTIZ~F$vcS}x!m?_676&/xW_x}5kTQ5w' );
define( 'AUTH_SALT',        'XWYH-H&qrUBN{Fao,RfH[c@h_t(:@[GjoJ|A/sk_O;/<HB`ghcnBsk[>&[Z0u]|8' );
define( 'SECURE_AUTH_SALT', '0ekkb4Q<ib,~{N{C,:P{{7S5w`zv8P4,{{zM]F0rk-CAtx7-j^yGF#vAs~Viu#@?' );
define( 'LOGGED_IN_SALT',   'AHG(p;P/I1@Hq2- <laJ`&7F>pcS<_C?-69QZq6c%BBLv^I@9nEH+R%PFe_8SRGb' );
define( 'NONCE_SALT',       'b*5@aV6N9:>?Lsp|nGER<gb#p=R:*)VKt#E*wQq.RA/z%02M3#6R~[& HBqlqo5M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
